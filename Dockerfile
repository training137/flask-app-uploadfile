FROM python:3.7.8-alpine3.11
WORKDIR /app
ADD requirements.txt .
RUN pip3 install -r requirements.txt
ADD . .
ENTRYPOINT ["python3","app.py"]